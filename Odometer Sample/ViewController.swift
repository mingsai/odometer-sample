//
//  ViewController.swift
//  Odometer Sample
//
//  Created by Heber Sheffield on 11/17/15.
//  Copyright © 2015 Polished Play. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var digit1000: OdometerSampleView!
    @IBOutlet weak var digit100: OdometerSampleView!
    @IBOutlet weak var digit10: OdometerSampleView!
    @IBOutlet weak var digit1: OdometerSampleView!
    
    
    @IBAction func actionRandom(sender: AnyObject) {
        let number = arc4random_uniform(10000)
        
        let thousands = Int((number / 1000) % 10)
        let hundreds = Int((number / 100) % 10)
        let tens = Int((number / 10) % 10)
        let ones = Int(number % 10)

        let speedDifference = (number % 2) > 0 // change to see different behavior
        if speedDifference {
            view.layer.speed = 0.4
            digit1000.layer.speed = 1
            digit100.layer.speed = 1
            digit10.layer.speed = 10
            digit1.layer.speed = 100
            
            scrollOdometer(digit1000, toDigit: thousands)
            scrollOdometer(digit100, toDigit: hundreds, afterCycles: thousands)
            scrollOdometer(digit10, toDigit: tens, afterCycles: hundreds)
            scrollOdometer(digit1, toDigit: ones, afterCycles: (hundreds * 10) + tens)
            
        } else {
            view.layer.speed = 1
            digit1000.layer.speed = 1
            digit100.layer.speed = 1
            digit10.layer.speed = 1
            digit1.layer.speed = 1
            
            scrollOdometer(digit1000, toDigit: thousands)
            scrollOdometer(digit100, toDigit: hundreds)
            scrollOdometer(digit10, toDigit: tens)
            scrollOdometer(digit1, toDigit: ones)
        }
            
        print(number)
    }
    
    func scrollOdometer(odometer: OdometerSampleView, toDigit digit: Int, afterCycles numberOfCycles: Int = 0) {
        if numberOfCycles > 0 {
            odometer.add_0Animation({ finished in
                if finished {
                    self.scrollOdometer(odometer, toDigit: digit, afterCycles: numberOfCycles - 1)
                }
            })
        } else {
            
            switch(digit) {
            case 1: odometer.add_1Animation()
            case 2: odometer.add_2Animation()
            case 3: odometer.add_3Animation()
            case 4: odometer.add_4Animation()
            case 5: odometer.add_5Animation()
            case 6: odometer.add_6Animation()
            case 7: odometer.add_7Animation()
            case 8: odometer.add_8Animation()
            case 9: odometer.add_9Animation()
            case 0: break
            default:
                assertionFailure("invalid digit: \(digit)")
                break
            }
        }

    }
    
    

}

